# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.2] - 2018-03-16
### Changed
- Code cleaned.

### Removed
- Useless matrix transform in step 2. This should significatiely increase performance. 

## [0.1.1] - 2017-03-15
### Changed
- No more Bcasting inside of nested loops. This should increase performance.

### Fixed
- Out of bound array error which could occure in the back substitution phase.

## [0.1.0] - 2017-02-20
Initial release.
