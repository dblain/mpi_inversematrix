# Mpi_matinv

## Synopsis

2D matrix inverse parallel algorithm. Use the Gaussian elimination method. See [this paper] (http://www.cse.buffalo.edu/faculty/miller/Courses/CSE633/thanigachalam-Spring-2014-CSE633.pdf) for a short explanation.
Code in Fortran. [OpenMPI] (https://www.open-mpi.org/) required.

## Code Example

This is a simple program that uses mpi_matinv to inverse a random 1000x1000 double precision matrix and display the result and the elapsed time:
```fortran
program main
    use mpi
    use mpi_utils, only: mpi_matinv, mpi_rank, mpi_nproc, mpi_ierr

    implicit none

    integer, parameter :: n = 1000 ! size of the matrix
    integer :: i
    double precision :: t0, tf
    double precision, dimension(n * n) :: array
    double precision, dimension(n, n) :: A, A_inv

    ! Initialize MPI (needed in all MPI codes)
    call MPI_INIT(mpi_ierr)

    ! Get the rank and the number of process (needed for mpi_matinv)
    call MPI_COMM_RANK(MPI_COMM_WORLD,mpi_rank,mpi_ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD,mpi_nproc,mpi_ierr)

    ! Fill the matrix to inverse with random numbers
    do i = 1, n * n
        array(i) = rand(0) * 1D0
    enddo
    A = transpose(reshape(array, shape(A)))
  
    ! Initialize the inversed matrix
    A_inv(:, :) = 0D0
    
    call cpu_time(t0) ! store start inversion time

    ! Do the inversion
    call mpi_matinv(A(:, :),A_inv(:, :), n)

    call cpu_time(tf) ! store end inversion time

    ! Check if the result is correct
    if (mpi_rank == 0) print *, 'result =', sum(matmul(A(:,:), A_inv(:,:))) / n  ! ideally = 1
    if (mpi_rank == 0) print *, 'elapsed time (s) = ', tf - t0

    ! Wait for all the process (avoid program ending before the display of the results)
    call MPI_BARRIER(MPI_COMM_WORLD, mpi_ierr)

    ! Put this at the end of all MPI codes
    call MPI_FINALIZE(mpi_ierr)
end program main
```

Compile with:
```
mpif90 module_mpi_utils.f90 main.f90 -o example.mpi
```

If executed with:
```
mpiexec -n 4 ./example.mpi
```

Then the output should looks like this:
```
result =   1.0000000008237773 
elapsed time (s) =    1.0440000000000000
```

## Motivation

This provides an easy way to implement a parallelised matrix inversion into a fortran program.
With 4 processes, this code should be ~45 % faster than a [LU algortihm] (http://ww2.odu.edu/~agodunov/computing/programs/book2/Ch06/Inverse.f90). The scaling should be pretty good, the computational time being divided by the number of process.

**Disclamer: this program is _not_ an optimised algorithm. It is important to notice that more performant algorithms exists, such as [Eigen] (http://eigen.tuxfamily.org/index.php?title=Main_Page) or [scaLapack] (http://www.netlib.org/scalapack/). Please consider using one of them before using this program.**

## Installation

Download module_mpi_utils.f90 and compile it with your code, as showed in the example section.
